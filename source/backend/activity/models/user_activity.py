from django.contrib.auth import get_user_model
from django.db import models

from activity.models import Activity


class UserActivity(models.Model):
    activity = models.ForeignKey(
        Activity, null=False, on_delete=models.CASCADE, related_name='user_activities'
    )
    user = models.ForeignKey(get_user_model(), on_delete=models.RESTRICT, blank=False, null=False)
    updated = models.DateTimeField(auto_now=True, null=False)
    done = models.BooleanField(default=False, null=False)
