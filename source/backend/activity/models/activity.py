from django.db import models
from rest_framework import serializers


class Activity(models.Model):
    key = models.IntegerField(null=False, primary_key=True)
    activity = models.CharField(max_length=144, null=False, blank=True)
    type = models.CharField(max_length=64, null=False, blank=True)
    participants = models.IntegerField(null=False)
    price = models.FloatField(null=False, )
    link = models.CharField(max_length=2150, null=False, blank=True)
    accessibility = models.FloatField(null=False)
    created = models.DateTimeField(auto_now_add=True, null=False)


class ActivitySerializer(serializers.ModelSerializer):
    done = serializers.SerializerMethodField()

    class Meta:
        model = Activity
        fields = [
            'key',
            'activity',
            'type',
            'participants',
            'price',
            'link',
            'accessibility',
            'created',
            'done',
        ]

        read_only_fields = [
            'created',
        ]

    def get_done(self, obj: Activity):
        if hasattr(obj, 'users') and len(obj.users) > 0:
            return obj.users[0].done
        else:
            return False

