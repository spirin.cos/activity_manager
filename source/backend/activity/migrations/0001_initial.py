# Generated by Django 4.1.5 on 2023-02-01 06:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('key', models.IntegerField(primary_key=True, serialize=False)),
                ('activity', models.CharField(blank=True, max_length=144)),
                ('type', models.CharField(blank=True, max_length=64)),
                ('participants', models.IntegerField()),
                ('price', models.FloatField()),
                ('link', models.CharField(blank=True, max_length=2150)),
                ('accessibility', models.FloatField()),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='UserActivity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('updated', models.DateTimeField(auto_now=True)),
                ('done', models.BooleanField(default=False)),
                ('activity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_activities', to='activity.activity')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
