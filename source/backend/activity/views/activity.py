from typing import Optional

from django.db.models import Prefetch
from django.http import Http404, JsonResponse
from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, status
from rest_framework.request import Request
from rest_framework.viewsets import GenericViewSet

from activity.lib.boredapi_adapter import bored_api_adapter
from activity.models import Activity, ActivitySerializer, UserActivity
from core.paginations import StandardResultsSetPagination


@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_summary="Get all user's activities", tags=['Activity'],
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    operation_summary='Create the new activities for user', tags=['Activity'],
    request_body=openapi.Schema(type=openapi.TYPE_OBJECT, properties={}),
))
@method_decorator(name='mark_as_finished', decorator=swagger_auto_schema(
    operation_summary='Mark the activity as finished', tags=['Activity'],
    request_body=openapi.Schema(type=openapi.TYPE_OBJECT, properties={})
))
class ActivityViewSet(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet
):

    model_class = Activity
    queryset = model_class.objects.order_by('-key')
    pagination_class = StandardResultsSetPagination
    lookup_field = 'key'
    serializer_class = ActivitySerializer

    def get_queryset(self):
        queryset = super(ActivityViewSet, self).get_queryset()
        return queryset.filter(user_activities__user_id=self.request.user.id).prefetch_related(
            Prefetch('user_activities',
                     to_attr='users', queryset=UserActivity.objects.filter(user_id=self.request.user.id))
        ).all()

    def create(self, request: Request, *args, **kwargs) -> JsonResponse:
        activity_data: dict = bored_api_adapter.get_new_activity()

        activity: Optional[Activity] = self.model_class.objects.filter(
            key=activity_data.get(self.lookup_field)
        ).first()

        if activity:
            serializer: ActivitySerializer = ActivitySerializer(instance=activity, data=activity_data)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            UserActivity.objects.get_or_create(activity=serializer.instance, user=self.request.user)
        else:
            serializer: ActivitySerializer = self.get_serializer(data=activity_data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            user_activity = UserActivity(activity=serializer.instance, user=self.request.user)
            user_activity.save()

        headers = self.get_success_headers(serializer.data)
        return JsonResponse(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def mark_as_finished(self, request: Request, key: int) -> JsonResponse:
        activity: Optional[Activity] = self.get_queryset().filter(pk=key).first()
        if not activity:
            raise Http404(f'{key} activity not exist')
        user_activity: UserActivity = activity.users[0]
        user_activity.done = True
        user_activity.save()
        serializer = self.serializer_class(instance=activity)
        return JsonResponse(data=serializer.data)
