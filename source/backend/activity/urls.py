from django.urls import path

from activity.views.activity import ActivityViewSet

urlpatterns = [
    path(r'activities/', ActivityViewSet.as_view({'post': 'create', 'get': 'list'}), name='action_create'),
    path(r'activities/<int:key>/', ActivityViewSet.as_view({'patch': 'mark_as_finished'}), name='action_create2'),
]