import logging
import urllib
from abc import ABC

import requests
from requests import Response, Session
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from activity.lib.exceptions import BaseRequestError
from django.conf import settings

def is_success(code):
    return 200 <= code <= 299


class BaseAdapter(ABC):
    base_url: str
    _http: Session
    exception_class = BaseRequestError

    def __init__(self, base_url: str):
        retry_strategy = Retry(
            total=3,
            status_forcelist=[429, 500, 502, 503, 504],
            allowed_methods=['GET', 'POST'],
            backoff_factor=3,
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)

        self.base_url = base_url
        self._http = requests.session()
        self._http.mount('https://', adapter)
        self._http.mount('http://', adapter)

    def _get_absolute_urls(self, relative_url: str):
        return urllib.parse.urljoin(self.base_url, relative_url)

    def _get_base_headers(self) -> dict:
        return {}

    def request(self, method: str, url: str, *args, **kwargs) -> Response:
        input_headers = kwargs.pop('headers', {})
        headers = {**input_headers, **self._get_base_headers(), **self._http.headers}
        return self._request(method, url, headers=headers, *args, **kwargs)

    def _handle_request_error(self, response: Response):
        pass

    def _request(self, method: str, url: str, *args, **kwargs) -> Response:
        absolute_url = self._get_absolute_urls(url)

        try:
            response = self._http.request(method, absolute_url, *args, **kwargs, )
            if not is_success(response.status_code):
                error_message = f'error request {response.status_code} {method} {absolute_url}:\n{response.text}'
                logging.error(error_message)
                self._handle_request_error(response)
                raise self.exception_class(error_message)
        except Exception as err:
            raise self.exception_class(err)
        return response

    def post(self, url: str, *args, **kwargs) -> Response:
        return self.request('post', url, *args, **kwargs)

    def get(self, url: str, *args, **kwargs) -> Response:
        return self.request('get', url, *args, **kwargs)

    def delete(self, url: str, *args, **kwargs) -> Response:
        return self.request('delete', url, *args, **kwargs)


class BoredApiAdapter(BaseAdapter):
    def get_new_activity(self) -> dict:
        url = '/api/activity'
        return self.get(url).json()


bored_api_adapter = BoredApiAdapter(base_url=settings.BASE_BORED_API_URL)
