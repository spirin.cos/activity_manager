# Activity Manager

A Django application that gives recommendations to a user about
what to do in their free time.
___

## Run locally

1. Create a virtual environment (Python version 3.8 is preferred):


    python3 -m venv venv

2. Activate the virtual environment and install dependencies


    source venv/bin/activate
    
    pip install -r requirements.txt


3. Fill in the env file:


    vim example.env

    set -o allexport; source example.env; set +o allexport 

4. Roll out migrations for the database


    cd ./source/backend 

    python3 manage.py collectstatic

    python3 manage.py migrate

    python3 manage.py createsuperuser

5. Start the debug server


    python3 manage.py runserver